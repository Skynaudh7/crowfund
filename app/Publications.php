<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publications extends Model
{
    // Nom de la table
    // protected $table = 'publications';
    
    // Clé primaire
    // public $primaryKey = 'id';

    // /timestamps
    // public $timestamps = false;

    protected $dates = ['dateLimite'];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
