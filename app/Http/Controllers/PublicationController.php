<?php

namespace App\Http\Controllers;

use App\Publications;
use Illuminate\Http\Request;

class PublicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $Publications = Publications::all();
        $Publications = Publications::orderBy('created_at', 'desc')->get();
        return view('publications.index')->with('publications', $Publications);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('publications.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'titre' => ['required', 'string'],
            'contenu' => ['required', 'string'],
            'dateLimite' => ['required', 'date'],
            'numeroCompte' => ['required', 'string'],
            'montant' => ['required', 'integer'],
            'image' => 'image|nullable|max:1999'
        ]);

        if($request->hasFile('image')){
            $filenameWithExt = $request->file('image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();

            $fileNameToStore = $filename .'_'.time().'.'.$extension;

            $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        }
        else{
            $fileNameToStore = 'noimage.jpeg';
        }

        $Publication = new Publications;
        $Publication->titre = $request->input('titre');
        $Publication->contenu = $request->input('contenu');
        $Publication->dateLimite = $request->input('dateLimite');
        $Publication->numeroCompte = $request->input('numeroCompte');
        $Publication->montant = $request->input('montant');
        $Publication->image = $fileNameToStore;
        $Publication->user_id = auth()->user()->id;
        $Publication->save();

        return redirect('/')->with('success', 'Projet publié avec success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Publication = Publications::find($id);
        return view('publications.show')->with('publications', $Publication);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd($id);
        $Publication = Publications::find($id);
        dd($id);
        $Publication->delete();

        return redirect('/Publications')->with('success', 'Suppression réussie!');
    }
}
