@extends('layouts.app')

@section('content')
<div class="">
    <div class="justify-content-center">
        <div>
            <div class="card">
                <div class="card-header">{{ Auth::user()->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- {{ __('You are logged in!') }} --}}
                    <a href="/Publications/create" class="btn btn-primary">Poster votre projet</a>
                    <h3 class="px-5">Liste de vos différents Projets</h3>
                    @if (count($publications) > 0)
                        <table class="table table-striped">
                            <tr>
                                <th>Titre</th>
                                <th>Description</th>
                                <th>Date de creation</th>
                                <th>Actions</th>
                            </tr>
                            @foreach ($publications as $publication)
                            <tr class="sizing">
                                <td>{{$publication->titre}}</td>
                                <td>{{$publication->contenu}}</td>
                                <td>{{$publication->created_at->format('d/m/Y')}}</td>
                                <td><a href="/Publications/{{$publication->id}}" class="btn btn-info">Aperçue</a> <a href="/Publications/{{$publication->id}}/edit" class="btn btn-warning">Editer</a>  <a href="/Publications/{{$publication->id}}/destroy" class="btn btn-danger">Delete</a></td>
                            </tr>
                                
                            @endforeach
                        </table>
                    @else
                    <div class="alert alert-danger">Vous n'avez publié aucun projet</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
