@extends('layouts.app')

@section('content')
<div class="">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Liste des abonnés à la plateforme</div>

                <div class="card-body">
                    @if (count($users) > 0)
                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                        @foreach ($users as $user)
                        <tr class="sizing">
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td><a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-warning">Editer</a>
                                <a href="{{ route('admin.users.destroy', $user->id) }}" class="btn btn-danger">Delete</a></td>
                        </tr>
                        @endforeach
                    </table>
                @else
                <div class="alert alert-danger">Aucun autre abonné à part vous!</div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
