@extends('layouts.app')

@section('content')
    <h1>Projets en cours</h1>
    @if (count($publications) > 0)
    <a href="{{ route('Publications.create')}}" class="btn btn-info">Poster Projet</a>
    <hr>
    
    @foreach ($publications as $publication)
    <div class="card">
        {{-- <ul class="list-group list-group-flush"> --}}

        <div class="row">
            <div class="col-md-4">
                <img style="width: 100%" src="/storage/images/{{$publication->image}}" alt="" >
            </div>
            <div class="col-md-8 alert-info">
                <h2 class="px-5"><a href="/Publications/{{$publication->id}}">{{$publication->titre}}</a></h2>
                <hr>
                <h4 class="px-5 justify-content-right">Description: <small class="alert-success">{{$publication->contenu}}</small></h4>
                <hr>
                Publié le <small class="alert-warning"> {{$publication->created_at}}</small>
                <a href="/Publications/{{$publication->id}}" class="btn btn-danger" style="float: right; margin-right:2%;">Aperçue</a>
            </div>
            
        </div>
    {{-- </ul> --}}
    </div>
    <hr>
    <hr>
    @endforeach

    @else
        <div class="alert alert-danger">Aucune publication ajoutée</div>
    @endif
@endsection