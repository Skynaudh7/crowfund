@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 alert-info">
            <div class="card alert-info">
                <div class="card-header alert-warning">Publier un nouveau Projet</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('Publications.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="name">Titre du projet</label>
                                <input id="titre" type="text" class="form-control @error('titre') is-invalid @enderror" name="titre" value="{{ old('titre') }}"  autocomplete="titre" autofocus>

                                @error('titre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="contenu">Contenu/Description du projet</label>
                                    <input id="contenu" type="textarea" class="form-control @error('contenu') is-invalid @enderror" name="contenu" value="{{ old('contenu') }}"  autocomplete="contenu" autofocus>
    
                                    @error('contenu')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="contenu">Date Limite du projet</label>
                                        <input id="dateLimite" type="date" class="form-control @error('dateLimite') is-invalid @enderror" name="dateLimite" value="{{ old('dateLimite') }}"  autocomplete="dateLimite" autofocus>
        
                                        @error('dateLimite')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="contenu">Numéro de Compte du Projet</label>
                                            <input id="numeroCompte" type="text" class="form-control @error('numeroCompte') is-invalid @enderror" name="numeroCompte" value="{{ old('numeroCompte') }}"  autocomplete="numeroCompte" autofocus>
            
                                            @error('numeroCompte')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="contenu">Montant du Projet</label>
                                                <input id="montant" type="number" maxlength="10" class="form-control @error('montant') is-invalid @enderror" name="montant" value="{{ old('montant') }}"  autocomplete="montant" autofocus>
                
                                                @error('montant')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                <div class="form-group">
                                    <div class="custom-file">
                                        <input type="file"  id="validatedCustomFile" name="image" class="custom-file-input
                                        @error('image') 
                                        is-invalid
                                        @enderror">
                                        <label for="validatedCustomFile" class="custom-file-label">Choisir image...</label>
                                        @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary">
                                    Creer Projet
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection