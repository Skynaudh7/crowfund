@extends('layouts.app')

@section('content')
<a href="/" class="btn btn-info">Retour</a>
    <h1>{{$publications->titre}}</h1>
    <div class="row">
        <div class="col-md-8 alert-info">
            <img style="width: 100%; height: 300px;" src="/storage/images/{{$publications->image}}" alt="" >
        </div>
        <div class="py-4 col-md-4">
            <h3>Informations Complémentaires</h3>
            <hr>
            <h5>Date limite pour le financement du projet: <small class="alert-danger">{{$publications->dateLimite->format('d/m/Y H:i')}}</small></h5>
            <hr>
            <h5>Numéro de compte du projet: <small class="alert-success">{{$publications->numeroCompte}}</small></h5>
            <hr>
            <h3>Montant du projet: <small class="alert-success">{{$publications->montant}}  FCFA</small></h3>
        </div>
    </div>
    <p>{{$publications->contenu}}</p>
    <hr>
    <small>Publié le {{$publications->created_at}}</small>
    {{-- <hr>
    <a href="#" class="btn btn-warning">Modifier</a>    <a href="{{ route('Publication.destroy', $publications->id)}}" class="btn btn-danger">Supprimer</a> --}}
@endsection