<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

// Route::get('/', '')
Route::get('/', 'HomeController@index')->name('home');
Route::resource('Publications', 'PublicationController');
Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function(){
    Route::resource('users', 'UsersController');
});








// Route::get('/Publications/create', 'PublicationController@create')->name('publication.create');
// Route::post('/Publications', 'PublicationController@store')->name('publication.store');
// Route::delete('/Publications/{id}', 'PublicationController@destroy')->name('publication.destroy');
