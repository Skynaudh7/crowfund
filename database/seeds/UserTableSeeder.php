<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Vider la table pivot <role,users> avec la methode truncate()
        User::truncate();
        DB::table('role_user')->truncate();

        // Création du seeder Admin

        $admin = User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('administrateur')
        ]);

        // Création du seeder Donateur

        $donateur = User::create([
            'name' => 'donateur',
            'email' => 'donateur@gmail.com',
            'password' => Hash::make('donateur')
        ]);

         // Création du seeder Utilisateurs
        $utilisateur = User::create([
            'name' => 'utilisateur',
            'email' => 'utilisateur@gmail.com',
            'password' => Hash::make('utilisateur')
        ]);


        // Recuperer les roles pour les attacher aux utilisateurs
        $adminRole = Role::where('name', 'admin')->first();
        $donateurRole = Role::where('name', 'donateur')->first();
        $utilisateurRole = Role::where('name', 'utilisateur')->first();

        // Attacher les roles aux Utilisateurs
        $admin->roles()->attach($adminRole);
        $donateur->roles()->attach($donateurRole);
        $utilisateur->roles()->attach($utilisateurRole);
    }
}
